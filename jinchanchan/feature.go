package jinchanchan

import (
	"auto_task.com/base/screen"
	"auto_task.com/base/utils"
	"auto_task.com/base/utils/fileutil"
	"auto_task.com/base/utils/log"
	"auto_task.com/jinchanchan/cmd/display"
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"io/ioutil"
	"sort"
	"sync"
	"time"
)

var (
	once sync.Once
)

type Feature struct {
	Name string
	RGB  [][3]uint8
	Gray []int
	Desc string
}

func NewFeature() *Feature {
	return &Feature{}
}

func (f *Feature) Append(input [3]uint8) {
	f.RGB = append(f.RGB, input)
}

func (f *Feature) AppendRGB(r, g, b uint8) {
	if r == 0 && g == 0 && b == 0 {
		return
	}
	//转灰度
	avg := float32(r)*0.299 +
		float32(g)*0.587 +
		float32(b)*0.114
	f.Append([3]uint8{r, g, b})
	f.Gray = append(f.Gray, int(avg))
}

type Point struct {
	X int
	Y int
}

func NewPoint(x, y int) *Point {
	return &Point{X: x, Y: y}
}

type FeatureLib struct {
	Items []*FeatureLibItem
}

type FeatureLibItem struct {
	Name    string
	Feature []int
	Desc    string
}

func SaveFeatures(features []*Feature) error {

	libFileName := ""
	i := 1
	for {
		libFileName = fmt.Sprintf("./lib/feature_%v.json", i)
		if exists := fileutil.HasFile(libFileName); !exists {
			break
		}
		i++
	}

	featureData, _ := ioutil.ReadFile(libFileName)
	if len(featureData) == 0 {
		featureData = []byte("{}")
	}

	lib := &FeatureLib{}
	err := json.Unmarshal(featureData, lib)
	if err != nil {
		return err
	}

	lib.Items = append(lib.Items, ConvFeaturesToItems(features)...)

	featureJson, _ := json.MarshalIndent(lib, "", " ")

	err = ioutil.WriteFile(libFileName, featureJson, 0777)
	if err != nil {
		return err
	}

	return nil
}

func ConvItemToFeature(item *FeatureLibItem) *Feature {

	f := &Feature{
		Name: item.Name,
		Gray: item.Feature,
		Desc: item.Desc,
	}

	return f

}

func ConvFeaturesToItems(fs []*Feature) []*FeatureLibItem {
	items := make([]*FeatureLibItem, 0)

	for _, f := range fs {
		items = append(items, ConvFeatureToItem(f))
	}

	return items
}

func ConvFeatureToItem(f *Feature) *FeatureLibItem {
	if len(f.RGB) == 0 {
		return nil
	}

	log.Infof("ConvFeatureToItem Gray:%+v", f.Gray)
	//只要灰度的
	item := &FeatureLibItem{Name: f.Name, Feature: f.Gray, Desc: f.Desc}

	return item

}

func calFeatureCore(img *image.RGBA) (*Feature, error) {

	log.Infof("bound x:%v y:%v", img.Bounds().Dx(), img.Bounds().Dy())

	//特征：

	//lib: 一个特征库，表示某个图片对应的特征序列。
	// 特征序列: 一个数组。

	coreSize := 5
	width := img.Bounds().Dx()
	height := img.Bounds().Dy()

	feature := NewFeature()
	for i := 0; i < height-coreSize; i += coreSize {
		for j := 0; j < width-coreSize; j += coreSize {
			r1, g1, b1, _ := img.At(i, j+2).RGBA()
			r2, g2, b2, _ := img.At(i+2, j).RGBA()
			r3, g3, b3, _ := img.At(i+2, j+2).RGBA()
			r4, g4, b4, _ := img.At(i+2, j+4).RGBA()
			r5, g5, b5, _ := img.At(i+4, j+2).RGBA()
			rAvg := (r1 + r2 + r3 + r4 + r5) / 5
			gAvg := (g1 + g2 + g3 + g4 + g5) / 5
			bAvg := (b1 + b2 + b3 + b4 + b5) / 5
			feature.AppendRGB(uint8(rAvg), uint8(gAvg), uint8(bAvg)) //转成255
		}
	}

	return feature, nil
}

func CalFeature(cardIdx int, x1, y1, x2, y2 int) (*Feature, error) {

	img, err := screen.ShotRectOrig(x1, y1, x2, y2)
	if err != nil {
		return nil, err
	}

	log.Printf("ShotRectOrig w:%v h:%v", 445-318, 744-650)

	filename := fmt.Sprintf("./asset/%v_%v.jpg", utils.FormatSafe(time.Now()), cardIdx)
	err = screen.SaveToLocal(img, filename)
	if err != nil {
		return nil, err
	}

	feature, err := calFeatureCore(img)
	if err != nil {
		return nil, err
	}

	feature.Name = ""
	feature.Desc = filename

	return feature, nil
}

func Learn(title string) error {
	leftTopX := 318
	leftTopY := 650
	cardWidth := 129
	cardHeight := 94
	jiange := 5
	features := make([]*Feature, 0)
	for i := 0; i < 5; i++ {
		rightButtomX := leftTopX + cardWidth
		rightButtomY := leftTopY + cardHeight
		feature, err := CalFeature(i, leftTopX, leftTopY, rightButtomX, rightButtomY)
		if err != nil {
			return err
		}
		feature.Name = fmt.Sprintf("%v_%v", title, i)
		features = append(features, feature)

		leftTopX += cardWidth + jiange
	}

	err := SaveFeatures(features)
	if err != nil {
		return err
	}

	return nil
}

func Compare(featureA *Feature, featureB *Feature) (int, error) {
	if len(featureA.Gray) != len(featureB.Gray) {
		return 0, errors.New("特征点数量不同")
	}

	scoreSum := 0

	scoreMap := make(map[int]int, 0)

	for i := range featureA.Gray {
		score := int((featureA.Gray[i]-featureB.Gray[i])*100/featureB.Gray[i]) / 100
		scoreSum += score
		scoreMap[score]++
	}

	if featureA.Name[1:] == featureB.Name[1:] {
		log.Infof("len(socreMap):%v", len(scoreMap))
		for k, v := range scoreMap {
			log.Infof("\tk:%v v:%v", k, v)
		}
	}

	{
		//区间统计
		if featureA.Name[1:] == featureB.Name[1:] && len(scoreMap) > 1 {
			aAreaMap := make(map[int]int)
			bAreaMap := make(map[int]int)
			for _, gray := range featureA.Gray {
				aAreaMap[gray/5*5]++
			}
			for _, gray := range featureB.Gray {
				bAreaMap[gray/5*5]++
			}

			type Statistic struct {
				Rank int
				Num  int
			}

			aStatistics := make([]Statistic, 0)
			bStatistics := make([]Statistic, 0)

			for k, v := range aAreaMap {
				aStatistics = append(aStatistics, Statistic{
					Rank: k,
					Num:  v,
				})
			}
			sort.Slice(aStatistics, func(i, j int) bool {
				return aStatistics[i].Rank > aStatistics[j].Rank
			})

			for k, v := range bAreaMap {
				bStatistics = append(bStatistics, Statistic{
					Rank: k,
					Num:  v,
				})
			}
			sort.Slice(bStatistics, func(i, j int) bool {
				return bStatistics[i].Rank > bStatistics[j].Rank
			})

			diffAll := 0
			for i := range aStatistics {
				diff := aStatistics[i].Num - bAreaMap[aStatistics[i].Rank]
				if diff < 0 {
					diff = -diff
				}
				diffAll += diff
				log.Infof("rank:%v diff:%v a:%v b:%v ", aStatistics[i].Rank,
					diff, aStatistics[i].Num, bAreaMap[aStatistics[i].Rank])
				delete(bAreaMap, aStatistics[i].Rank)
			}

			for _, v := range bAreaMap {
				diffAll += v
			}
			log.Infof("rankDiffAll:%v", diffAll)

		}
	}

	//{ //灰度差值
	//	if featureA.Name[1:] == featureB.Name[1:] {
	//		grayDiffs := make([]float64, 0)
	//		grayDiffsInt := make([]int, 0)
	//		for i := range featureA.Gray {
	//			diff := featureA.Gray[i] - featureB.Gray[i]
	//			if diff < 0 {
	//				diff = - diff
	//			}
	//			grayDiffs = append(grayDiffs, float64(diff))
	//			grayDiffsInt = append(grayDiffsInt, diff)
	//		}
	//
	//		log.Infof("featureA: %v", utils.FormatIntSlice(featureA.Gray))
	//		log.Infof("featureB: %v", utils.FormatIntSlice(featureB.Gray))
	//		log.Infof("grayDiffs:%v", utils.FormatIntSlice(grayDiffsInt))
	//
	//		title := fmt.Sprintf("%v_vs_%v", featureA.Name, featureB.Name)
	//		show(title, utils.ParseIntSliceToFloat(featureA.Gray), utils.ParseIntSliceToFloat(featureB.Gray), grayDiffs)
	//	}
	//
	//}

	//{
	//	ratiosA := make([]float64, 0)
	//	ratiosB := make([]float64, 0)
	//	ratiosAStr := make([]string, 0)
	//	ratiosBStr := make([]string, 0)
	//	//得到一个斜率变化序列
	//	for i := range featureA.Gray {
	//		if i == len(featureA.Gray)-1 {
	//			continue
	//		}
	//		ratio := float64(featureA.Gray[i+1] - featureA.Gray[i]) / float64(featureA.Gray[i])
	//
	//		ratioStr := strconv.FormatFloat(ratio,'f',2,64)
	//		ratiosAStr = append(ratiosAStr, ratioStr)
	//
	//		ratio, _ = strconv.ParseFloat(ratioStr, 64)
	//		ratiosA = append(ratiosA, ratio)
	//	}
	//	for i := range featureB.Gray {
	//		if i == len(featureB.Gray)-1 {
	//			continue
	//		}
	//		ratio := float64(featureB.Gray[i+1] - featureB.Gray[i]) / float64(featureB.Gray[i])
	//
	//		ratioStr := strconv.FormatFloat(ratio,'f',2,64)
	//		ratiosBStr = append(ratiosBStr, ratioStr)
	//
	//		ratio, _ = strconv.ParseFloat(strconv.FormatFloat(ratio,'f',1,64), 64)
	//		ratiosB = append(ratiosB, ratio)
	//	}
	//
	//	if featureA.Name[1:] == featureB.Name[1:] {
	//
	//		title := fmt.Sprintf("%v_vs_%v", featureA.Name, featureB.Name)
	//		diffs := make([]float64, 0)
	//		for i := range ratiosA {
	//			diff := ratiosA[i] - ratiosB[i]
	//			if diff < 0 {
	//				diff = -diff
	//			}
	//			diffs = append(diffs, diff)
	//		}
	//		log.Infof("ratiosA:%v", ratiosAStr)
	//		log.Infof("ratiosB:%v", ratiosBStr)
	//		log.Infof("diffs:%v", diffs)
	//
	//		show(title, ratiosA, ratiosB, diffs)
	//	}
	//}

	return scoreSum / len(featureA.Gray), nil

}

func show(title string, a, b, c []float64) {

	log.Infof("show title:%v", title)

	tests := []struct {
		title     string
		endTime   time.Time
		barValues []display.LineYValue
	}{
		{title, time.Now(), []display.LineYValue{
			{"a", a},
			{"b", b},
			{"diff", c},
		},
		},
	}

	for _, test := range tests {
		_, err := display.CreateLineChart("show", test.title, test.endTime, test.barValues)
		if err != nil {
			log.Errorf("show CreateLineChart err:%v", err)
		}
	}

}
