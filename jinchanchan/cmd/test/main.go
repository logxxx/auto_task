package main

import (
	"auto_task.com/base/utils/fileutil"
	"auto_task.com/base/utils/log"
	"auto_task.com/jinchanchan"
	"encoding/json"
	"fmt"
	"io/ioutil"
)

func main() {

	fileutil.CleanDir("./show")

	features := make([]*jinchanchan.Feature, 0)
	i := 0
	for {
		i++
		fileName := fmt.Sprintf("../lib/feature_%v.json", i)
		if !fileutil.HasFile(fileName) {
			break
		}
		featureLibJson, _ := ioutil.ReadFile(fileName)
		if len(featureLibJson) == 0 {
			panic(i)
		}

		lib := &jinchanchan.FeatureLib{}

		err := json.Unmarshal(featureLibJson, lib)
		if err != nil {
			panic(err)
		}

		for _, item := range lib.Items {
			features = append(features, jinchanchan.ConvItemToFeature(item))
		}

	}

	log.Infof("共读入%v个特征。", len(features))

	for i := range features {
		for j := range features {
			if j <= i {
				continue
			}
			score, err := jinchanchan.Compare(features[i], features[j])
			if err != nil {
				panic(err)
			}

			same := ""
			if features[i].Name[1:] == features[j].Name[1:] {
				same = "[SAME]"
			}
			log.Printf("%v[%v]VS[%v] %v VS %v score:%v", same, i, j, features[i].Name, features[j].Name, score)
			log.Printf("desc:%v", features[i].Desc)
			log.Printf("desc:%v", features[j].Desc)
		}
	}

}
