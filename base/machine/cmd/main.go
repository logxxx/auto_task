package main

import (
	"auto_task.com/base/machine"
	"auto_task.com/base/screen"
	"auto_task.com/base/utils/log"
	"io/ioutil"
	"time"
)

func main() {
	for {
		time.Sleep(3 * time.Second)
		log.Infof("start!")
		screenData, err := screen.ShotAreaAndResize(0, 30, 1260, 740, 624, 366)
		if err != nil {
			panic(err)
		}
		//fileName := fmt.Sprintf("./base/machine/cmd/%v.jpg", time.Now().Format("20060102_150405"))
		//err = ioutil.WriteFile(fileName, screenData, 0777)
		//if err != nil {
		//	panic(err)
		//}

		result, err := machine.AskMachine(screenData)
		if err != nil {
			panic(err)
		}
		log.Printf("result:%+v", result)

	}
}

func main1() {
	file, err := ioutil.ReadFile("./base/machine/cmd/cat.2495.jpg")
	if err != nil {
		panic(err)
	}

	_, err = machine.AskMachine(file)
	if err != nil {
		panic(err)
	}
}
