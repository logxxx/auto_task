package machine

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type AskMachineResult struct {
	Result bool   `json:"result"`
	Prob   string `json:"prob"`
}

func AskMachine(data []byte) (*AskMachineResult, error) {
	machineUrl := "http://127.0.0.1:5000/my_upload"
	reqBuf := bytes.NewBuffer(data)
	//reqBuf := bytes.NewBufferString(screenDataB64)
	log.Printf("len(data):%v", len(data))

	req, err := http.NewRequest("POST", machineUrl, reqBuf)
	if err != nil {
		return nil, err
	}

	respHttp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer respHttp.Body.Close()

	respByte, err := ioutil.ReadAll(respHttp.Body)
	if err != nil {
		return nil, err
	}

	//log.Printf("CaptureAndSendToMachine respHttp:%v", string(respByte))

	resp := &AskMachineResult{}
	err = json.Unmarshal(respByte, resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
