package ocr

import (
	"encoding/base64"
	"fmt"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/errors"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
	ocr "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/ocr/v20181119"
	"strings"
)

type OCRResp struct {
	rawResp *ocr.GeneralAccurateOCRResponse
}

func OCR(data []byte) (resp OCRResp) {
	rawResp, err := ocrCore(data)
	if err != nil {
		return
	}
	resp.rawResp = rawResp
	return
}

func (o *OCRResp) Texts() []string {

	if o.rawResp == nil {
		return nil
	}

	texts := make([]string, 0)
	for _, row := range o.rawResp.Response.TextDetections {
		texts = append(texts, *row.DetectedText)
	}

	return texts
}

func (o *OCRResp) Contains(input string) bool {
	for _, text := range o.Texts() {
		if strings.Contains(text, input) {
			return true
		}
	}
	return false
}

func (o *OCRResp) Equal(input string) bool {
	for _, text := range o.Texts() {
		if text == input {
			return true
		}
	}
	return false
}

func ocrCore(data []byte) (*ocr.GeneralAccurateOCRResponse, error) {
	credential := common.NewCredential(
		"AKID786rUuyzKTQM6ecGM8kDJDwToNkx6Vjo",
		"50hMsEMK7f1FD90Xs8GEwIQHEuJVUHzY",
	)
	cpf := profile.NewClientProfile()
	cpf.HttpProfile.Endpoint = "ocr.tencentcloudapi.com"
	client, err := ocr.NewClient(credential, "ap-guangzhou", cpf)
	if err != nil {
		fmt.Printf("ocrCore NewClient err:%v", err)
		return nil, err
	}

	b64 := base64.StdEncoding.EncodeToString(data)

	request := ocr.NewGeneralAccurateOCRRequest()
	request.ImageBase64 = &b64

	response, err := client.GeneralAccurateOCR(request)
	if err2, ok := err.(*errors.TencentCloudSDKError); ok {
		fmt.Printf("An API error has returned: %s", err)
		return nil, err2
	}
	if err != nil {
		return nil, err
	}
	fmt.Printf("%s", response.ToJsonString())

	return response, nil
}
