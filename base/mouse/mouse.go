package mouse

import (
	"auto_task.com/base/utils"
	"github.com/go-vgo/robotgo"
)

var sleep = func() {
	utils.SleepSec(1)
}

func MoveAndClick(x, y int) {
	defer sleep()
	Move(x, y)
	Click()
}

func MoveAndManyClick(x, y, count int) {
	defer sleep()
	Move(x, y)
	for i := 0; i < count; i++ {
		Click()
	}
}

func Move(x, y int) {
	robotgo.Move(x, y)
}

func Click() {
	click(false)
}

func DoubleClick() {
	click(false)
	click(false)
}

func ClickRight() {
	click(true)
}

func click(isRight bool) {
	if isRight {
		robotgo.Click("right")
	} else {
		robotgo.Click()
	}
}

func Pos() (int, int) {
	return robotgo.GetMousePos()
}

func Toggle(sourceX, sourceY, destX, destY int) {
	defer sleep()
	Move(sourceX, sourceY)
	robotgo.Toggle("")
	Move(destX, destY)
	robotgo.Toggle("", "up")
}
