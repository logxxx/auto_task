package utils

import (
	"context"
	"log"
	"time"
)

func FormatDateSafe() string {
	return time.Now().Format("20060102")
}

func FormatNowSafe() string {
	return FormatSafe(time.Now())
}

func FormatSafe(t time.Time) string {
	return t.Format("20060102_150405")
}

func Format(t time.Time) string {
	return t.Format("2006/01/02 15:04:05")
}

func SleepSec(sec int) {
	Tick(context.Background(), time.Duration(sec)*time.Second)
}

func SleepAndDo(ctx context.Context, name string, d time.Duration, f func(ctx context.Context)) {
	deadline := time.Tick(d)
	endTime := time.Now().Add(d).Format("2006-01-02 15:04:05")
	round := 0
	for {
		select {
		case <-ctx.Done():
			return
		case <-time.After(10 * time.Second):
			round++
			secLeft := int(d.Seconds()) - round*5
			log.Printf("距离[%v]还有%v分%v秒:%v",
				name, secLeft/60, secLeft%60, endTime)
		case <-deadline:
			log.Printf("开始[%v]", name)
			if f != nil {
				f(ctx)
			}
			return
		}
	}
}

//防掉线，定期执行f
func TickWithFunc(ctx context.Context, d time.Duration, f func()) {
	if ctx == nil {
		ctx = context.Background()
	}
	deadline := time.Tick(d)
	round := 0
	for {
		select {
		case <-time.After(5 * time.Second):
			if f != nil {
				f()
			}
			round++
			log.Printf("sleep %v/%v", round*5, int(d.Seconds()))
		case <-deadline:
			return
		case <-ctx.Done():
			return
		}
	}
}

func Tick(ctx context.Context, d time.Duration) {
	TickWithFunc(ctx, d, nil)
}

func IsMonday() bool {
	return time.Now().Weekday() == time.Monday
}

func IsTuesday() bool {
	return time.Now().Weekday() == time.Tuesday
}

func IsWednesday() bool {
	return time.Now().Weekday() == time.Wednesday
}

func IsThursday() bool {
	return time.Now().Weekday() == time.Thursday
}

func IsFriday() bool {
	return time.Now().Weekday() == time.Friday
}

func IsSaturday() bool {
	return time.Now().Weekday() == time.Saturday
}

func IsSunday() bool {
	return time.Now().Weekday() == time.Monday
}
