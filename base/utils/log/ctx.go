package log

import (
	"context"
	"fmt"
	"log"
)

const (
	RequestId = "REQUEST_ID"
)

type CtxLog struct {
	ctx context.Context
}

func CtxWithRid(rid string) context.Context {
	return SetRid(context.Background(), rid)
}

func SetRid(ctx context.Context, rid string) context.Context {
	return context.WithValue(ctx, RequestId, rid)
}

func WithCtx(ctx context.Context) *CtxLog {
	return &CtxLog{ctx: ctx}
}

func (l *CtxLog) Infof(format string, v ...interface{}) {
	log.Printf(l.getPrefix()+format, v...)
}

func (l *CtxLog) Errorf(format string, v ...interface{}) {
	log.Printf(l.getPrefix()+format, v...)
}

func (l *CtxLog) getPrefix() string {
	prefix := "-"
	reqId := l.ctx.Value(RequestId)
	if reqId != nil {
		prefix = fmt.Sprintf("[rid=%v]", reqId)
	}
	return prefix
}
