package utils

import (
	"encoding/json"
	"fmt"
)

func ParseIntSliceToFloat(input []int) []float64 {
	output := make([]float64, 0)
	for _, num := range input {
		output = append(output, float64(num))
	}
	return output
}

func FormatIntSlice(input []int) string {
	formatted := make([]string, 0)

	for _, f := range input {
		formatted = append(formatted, fmt.Sprintf("%03d", f))
	}

	output, _ := json.Marshal(formatted)

	return string(output)
}

func FormatFloatSlice(input []float64) string {
	formatted := make([]string, 0)

	for _, f := range input {
		formatted = append(formatted, fmt.Sprintf("%f", f))
	}

	output, _ := json.Marshal(formatted)

	return string(output)
}
