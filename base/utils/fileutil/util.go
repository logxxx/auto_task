package fileutil

import (
	"auto_task.com/base/utils/log"
	"errors"
	"os"
	"strings"
)

func WriteToFile(data []byte, path string) error {

	//没有文件夹则尝试先创建
	elems := strings.Split(path, "/")
	if len(elems) < 1 {
		return errors.New("invalid path")
	}

	dir := strings.Join(elems[:len(elems)-1], "/")
	if !HasFile(dir) {
		err := os.MkdirAll(dir, 0777)
		if err != nil {
			log.Errorf("writeToFile MkdirAll err:%v", err)
			return err
		}
	}

	file, err := os.Create(path)
	if err != nil {
		log.Errorf("writeToFile Create err:%v", err)
		return err
	}
	defer file.Close()
	_, err = file.Write(data)
	if err != nil {
		log.Errorf("writeToFile Write err:%v", err)
		return err
	}

	return nil
}

func HasFile(path string) bool {
	if _, err := os.Stat(path); err != nil && os.IsNotExist(err) {
		return false
	}
	return true
}

func IsFieExist(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	//isnotexist来判断，是不是不存在的错误
	if os.IsNotExist(err) { //如果返回的错误类型使用os.isNotExist()判断为true，说明文件或者文件夹不存在
		return false, nil
	}
	return false, err //如果有错误了，但是不是不存在的错误，所以把这个错误原封不动的返回
}

func CleanDir(path string) {
	os.RemoveAll(path)
	os.Mkdir(path, 0777)
}
