package utils

func Abs(input int) int {
	if input < 0 {
		input = -input
	}
	return input
}

func IsInSlice(a int, s []int) bool {
	for _, elem := range s {
		if a == elem {
			return true
		}
	}
	return false
}
