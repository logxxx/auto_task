package action

import (
	"auto_task.com/base/utils/log"
	"time"
)

type WaitAction struct {
	Duration time.Duration
}

func NewWaitDuration(input string) (*WaitAction, error) {
	d, err := time.ParseDuration(input)
	if err != nil {
		return nil, err
	}
	return NewWait(d), nil
}

func NewWait(d time.Duration) *WaitAction {
	w := &WaitAction{Duration: d}
	return w
}

func (w *WaitAction) Do() {
	log.Infof("等待:%v", w.Duration.String())
	time.Sleep(w.Duration)
}
