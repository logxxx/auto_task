package action

import (
	"auto_task.com/base/mouse"
	"auto_task.com/base/utils/log"
)

type ClickAction struct {
	Name  string
	X     int
	Y     int
	Count int
}

func NewClick(x, y int) *ClickAction {
	return &ClickAction{X: x, Y: y, Count: 1}
}

func (c *ClickAction) Do() {
	log.Infof("点击:%v", c.Name)
	mouse.MoveAndManyClick(c.X, c.Y, c.Count)
}

func (c *ClickAction) SetName(name string) *ClickAction {
	c.Name = name
	return c
}

func (c *ClickAction) SetCount(count int) *ClickAction {
	if count <= 1 {
		return c
	}
	c.Count = count
	return c
}
