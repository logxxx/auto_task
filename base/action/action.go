package action

type Action interface {
	Do()
}

func Do(acts ...Action) {
	for _, act := range acts {
		act.Do()
	}
}
