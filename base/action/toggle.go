package action

import (
	"auto_task.com/base/mouse"
	"auto_task.com/base/utils/log"
)

type ToggleAction struct {
	X1 int
	Y1 int
	X2 int
	Y2 int
}

func NewToggle(x1, y1, x2, y2 int) *ToggleAction {
	t := &ToggleAction{
		X1: x1,
		Y1: y1,
		X2: x2,
		Y2: y2,
	}
	return t
}

func (t *ToggleAction) Do() {
	log.Infof("拖拽 (%v,%v)->(%v,%v)", t.X1, t.Y1, t.X2, t.Y2)
	mouse.Toggle(t.X1, t.Y1, t.X2, t.Y2)
}
