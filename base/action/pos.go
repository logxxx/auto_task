package action

import (
	"auto_task.com/base/mouse"
	"auto_task.com/base/utils/log"
	"time"
)

type Pos struct {
	fns  []func()
	Name string
	X    int
	Y    int
}

func NewPos(name string, x, y int) *Pos {
	return &Pos{Name: name, X: x, Y: y}
}

func (p *Pos) AddX(x int) {
	p.X += x
}

func (p *Pos) AddY(y int) {
	p.Y += y
}

func (p *Pos) Add(x, y int) {
	p.X += x
	p.Y += y
}

func (p *Pos) AppendFn(fn func()) {
	p.fns = append(p.fns, fn)
}

func (p *Pos) WithToggle(x1, y1, x2, y2 int) *Pos {

	p.AppendFn(func() {
		log.Printf("拖拽:%v", p.Name)
		mouse.Toggle(x1, y1, x2, y2)
	})

	return p
}

func (p *Pos) WithWait(sec int) *Pos {
	if sec <= 0 {
		return p
	}
	p.fns = append(p.fns, func() {
		log.Printf("卖单%v秒:%v", sec, p.Name)
		time.Sleep(time.Duration(sec) * time.Second)
	})
	return p
}

func (p *Pos) Click() {
	log.Printf("点击动作: %v", p.Name)
	mouse.MoveAndClick(p.X, p.Y)
}

func (p *Pos) ClickAndReturn() {
	x, y := mouse.Pos()
	log.Printf("点击动作: %v", p.Name)
	mouse.Move(p.X, p.Y)
	mouse.Click()
	mouse.Move(x, y)
}

func (p *Pos) DoubleClick() {
	log.Printf("双击动作: %v", p.Name)
	mouse.MoveAndManyClick(p.X, p.Y, 2)
}

func (p *Pos) ManyClick(count int) {
	log.Printf("点击%v次: %v", count, p.Name)
	mouse.MoveAndManyClick(p.X, p.Y, count)
}

func (p *Pos) Copy(posName string, x, y int) *Pos {
	if posName == "" {
		posName = p.Name
	}
	return &Pos{
		Name: posName,
		X:    p.X + x,
		Y:    p.Y + y,
	}
}

func ClickPos(pos *Pos) {
	log.Printf("点击动作: %v", pos.Name)
	mouse.MoveAndClick(pos.X, pos.Y)
}

func ClickPoss(poss ...*Pos) {
	for _, pos := range poss {
		for _, fn := range pos.fns {
			fn()
		}
		if pos.X > 0 && pos.Y > 0 {
			log.Printf("点击动作: %v", pos.Name)
			mouse.MoveAndClick(pos.X, pos.Y)
		}
	}
}

func WaitPos(Name string, sec int) *Pos {
	return &Pos{Name: Name, fns: []func(){func() {
		log.Printf("卖单%v秒:%v", sec, Name)
		time.Sleep(time.Duration(sec) * time.Second)
	}}}
}

func ToggleX(x1, y1, x2 int) *Pos {
	return PosToggle("", x1, y1, x2, y1)
}

func ToggleY(x1, y1, y2 int) *Pos {
	return PosToggle("", x1, y1, x1, y2)
}

func PosToggle(Name string, x1, y1, x2, y2 int) *Pos {
	return &Pos{Name: Name, fns: []func(){func() {
		log.Printf("拖拽:%v", Name)
		mouse.Toggle(x1, y1, x2, y2)
	}}}
}
