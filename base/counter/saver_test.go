package counter

import (
	"testing"
	"time"
)

func TestGetSaver(t *testing.T) {
	saver := GetCounter()
	now := time.Now().UnixNano()
	err := saver.Set("now", now, 0).Err()
	if err != nil {
		t.Fatal(err)
	}
	resp, err := saver.Get("now").Int64()
	if err != nil {
		t.Fatal(err)
	}
	if resp != now {
		t.Fatalf("resp != now. resp:%v now:%v", resp, now)
	}
}
