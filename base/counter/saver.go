package counter

import (
	"auto_task.com/base/utils/log"
	"gopkg.in/redis.v3"
	"time"
)

var (
	_counter *Counter
)

type Counter struct {
	*redis.Client
}

func init() {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "150.158.99.30:6379",
		Password: "he1234",
	})

	testPing := rdb.Ping().String()
	if testPing == "" {
		panic("rdb unreachable " + testPing)
	}
	log.Infof("init rdb succ")

	//保活
	go func() {
		for {
			rdb.Ping()
			time.Sleep(10 * time.Second)
		}
	}()
	_counter = &Counter{
		Client: rdb,
	}
}

func GetCounter() *Counter {
	return _counter
}
