package newer

import (
	"auto_task.com/base/action"
	"auto_task.com/base/utils"
)

//寻路中
//聊天
//领奖

// 跳过聊天
func SkipTalking() {
	action.ClickPos(action.NewPos("跳过聊天", 579, 657))
}

func RecvBonus() {
	action.ClickPos(action.NewPos("领取奖励", 1102, 663))
}

func Action_Caiji() {
	//也可以作为【使用】
	action.ClickPos(action.NewPos("采集", 901, 553))
}

func JustUseShenBing() {
	for {
		utils.SleepSec(10)
		action.NewPos("神兵", 855, 671).ClickAndReturn()
	}
}
