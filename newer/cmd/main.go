package main

import (
	"auto_task.com/base/utils/log"
	"auto_task.com/newer"
	"os"
)

func main() {

	wd, _ := os.Getwd()
	log.Infof("wd:%v", wd)

	//newer.JustUseShenBing()

	//go newer.CheckTransmit()

	go newer.CheckSomeSkillUnlocked()

	go newer.ClickFirstTask()

	go newer.CheckHuDongItem()

	go newer.CloseUseItemDialog()

	newer.CheckRecvBonus()

}
