package newer

import (
	"auto_task.com/base/screen"
	"auto_task.com/base/utils"
	"auto_task.com/base/utils/log"
	"fmt"
	"time"
)

func ClickFirstTask() {
	for {
		time.Sleep(3 * time.Second)
		clickFirstTaskCore()
	}
}

func clickFirstTaskCore() {
	//任务完成没完成： 红色和绿色
	FirstTask.ManyClick(3)
}

// 判断任务是否完成
func CheckIsTaskDone() error {
	//截取第一个任务的区域
	//判断区域内有红色还是绿色
	//红色为未完成，触发点击
	//绿色为完成(TODO:不知道干啥)
	x1 := 10
	y1 := 265
	x2 := 262
	y2 := 290
	img, err := screen.ShotRectOrig(10, 265, 262, 290)
	if err != nil {
		return err
	}

	filename := fmt.Sprintf("./asset/%v.jpg", utils.FormatSafe(time.Now()))
	err = screen.SaveToLocal(img, filename)
	if err != nil {
		return err
	}

	for i := 0; i < x2-x1; i++ {
		for j := 0; j < y2-y1; j++ {
			r, g, b, _ := img.At(i, j).RGBA()
			log.Infof("[%v, %v] (%v,%v,%v)", i+x1, j+y1, r, g, b)
		}
	}
	return nil
}
