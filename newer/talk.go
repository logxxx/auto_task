package newer

import (
	"auto_task.com/base/action"
	"auto_task.com/base/utils/color"
	"auto_task.com/base/utils/log"
	"time"
)

var (
	FirstTask = action.NewPos("点击第一个任务", 116, 266)

	ButtonRecvBonus      = action.NewPos("领奖", 1103, 668)
	ButtonHuDong         = action.NewPos("互动", 898, 554)
	ButtonTransmit       = action.NewPos("传送", 762, 420)
	ButtonUnlock         = action.NewPos("解锁", 631, 521)
	ButtonUseItem_Commit = action.NewPos("使用道具-确定", 738, 558)
	ButtonUseItem_Cancel = action.NewPos("使用道具-取消", 525, 556)
)

// 关闭【使用道具】弹窗
// 场景: 固本丹->被【道具互动】goroutine触发
func CloseUseItemDialog() {
	for {
		time.Sleep(5 * time.Second)
		ok := checkUseItemCore()
		if ok {
			log.Infof("可以使用道具！！！")
			ButtonUseItem_Cancel.Click()
		}
	}
}

func checkUseItemCore() bool {
	return color.CheckColor(451, 533, 601, 583, false, color.IsGreen)
}

// 检查是否可以传送
func CheckTransmit() {
	for {
		time.Sleep(5 * time.Second)
		ok := checkTransmitCore()
		if ok {
			log.Infof("可以传送！！！")
			ButtonTransmit.Click()
		}
	}
}

// 解锁了某个系统，要点【确定】才能退出
func CheckSomeSkillUnlocked() {
	for {
		time.Sleep(3 * time.Second)
		ok := checkSomeSkillUnlockedCore()
		if ok {
			log.Infof("可以解锁！！！")
			ButtonUnlock.Click()
		}
	}
}

//道具互动
func CheckHuDongItem() {
	for {
		time.Sleep(3 * time.Second)
		ok := checkHuDongItemCore()
		if ok {
			log.Infof("可以互动！！！")
			action.ClickPos(ButtonHuDong)
		}
	}

}

func CheckRecvBonus() {
	for {
		time.Sleep(2 * time.Second)
		ok := checkRecvBonusCore()
		if ok {
			log.Infof("可以领奖！！！")
			action.ClickPos(ButtonRecvBonus)
		}
	}
}

func checkTransmitCore() bool {
	return color.CheckColor(705, 398, 817, 440, false, color.IsYellow)
}

func checkSomeSkillUnlockedCore() bool {
	return color.CheckColor(576, 496, 691, 532, false, color.IsGreen)
}

// 判断是否可以领奖
func checkRecvBonusCore() bool {
	return color.CheckColor(1028, 638, 1176, 687, false, color.IsGreen, color.IsYellow)
}

func checkHuDongItemCore() bool {
	//NOTE:当有了新装备时，"装备"按钮的位置 和"互动"是一个。
	return color.CheckColor(841, 529, 954, 573, false, color.IsYellow)
}
