package main

import (
	"auto_task.com/base/utils"
	"auto_task.com/base/utils/log"
	"auto_task.com/config"
	"auto_task.com/pages"
	"context"
)

func main() {

	err := config.Init()
	if err != nil {
		panic(err)
	}

	//副本()
	//
	//return

	log.Infof("5s后开始...")
	utils.SleepSec(5)

	ctx, cancel := context.WithCancel(context.Background())

	//pages.Malling()

	//副本()

	任务()

	帮派()

	pages.CleanBag()

	杂务()
	领奖()

	pages.Farming(ctx)

	pages.Fishing()

	go pages.OutsizeHangup(ctx)
	utils.SleepSec(60 * 60 * 24)

	cancel()
}

func 杂务() {

	pages.BuyTongQian()

	pages.PK(false)

}

func 领奖() {

	pages.RecvHuoYueDuBonus()

	pages.RecvOnlineBonus()
}

func 副本() {
	pages.RunTeamFuBen()

	pages.RunSingleFuBen()
}

func 任务() {

	err := pages.DoTask()
	if err != nil {
		panic(err)
	}

	pages.WanShouGe()
}

func 帮派() {
	pages.Faction()

	pages.YunBiao(1)
	pages.YunBiao(2)

	pages.JumpToMonsterPoint()
}
