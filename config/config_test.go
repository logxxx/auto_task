package config

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestParseClick(t *testing.T) {
	req1 := "123,456"
	resp1, err := ParseClick(req1)
	assert.Nil(t, err)
	t.Logf("resp1:%+v", resp1)
	assert.Equal(t, resp1.X, 123)
	assert.Equal(t, resp1.Y, 456)

	req2 := "123,456+8"
	resp2, err := ParseClick(req2)
	assert.Nil(t, err)
	t.Logf("resp2:%+v", resp2)
	assert.Equal(t, resp2.X, 123)
	assert.Equal(t, resp2.Y, 456)
	assert.Equal(t, resp2.Count, 8)

}

func TestParseToggle(t *testing.T) {
	req1 := "123,456->234,567"
	resp1, err := ParseToggle(req1)
	assert.Nil(t, err)
	t.Logf("resp1:%+v", resp1)
	assert.Equal(t, 123, resp1.X1)
	assert.Equal(t, 456, resp1.Y1)
	assert.Equal(t, 234, resp1.X2)
	assert.Equal(t, 567, resp1.Y2)
}

func TestLoadConfig(t *testing.T) {

	wd, _ := os.Getwd()
	t.Logf("wd:%v", wd)

	cfg, err := LoadConfig("./config_test.yaml")
	assert.Nil(t, err)
	assert.NotNil(t, cfg)

	cfg.Print()
}
