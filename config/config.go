package config

import (
	"auto_task.com/base/utils/log"
	"encoding/json"
	"errors"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

//格式：[命令]:[目标]
//点击(123,456): clk:123,456
//点击(123,456)三次: clk:123,456+3
//从(123，456)拖拽到（234,567): tg:123,456>234,567
//等待3s: wt:3s
//等待1分钟: wt:1m

var (
	ActionClick  = "clk"
	ActionToggle = "tg"
	ActionWait   = "wt"

	errInvalidCommand = errors.New("invalid command")
)

var (
	globalCfg *GlobalConfig
)

type GlobalConfig struct {
	ShiLianTask *ShiLianTask `yaml:"ShiLianTask"`
}

type ShiLianTask struct {
	Skip  bool    `yaml:"Skip"`
	Tasks []*Task `yaml:"Tasks"`
}

type Task struct {
	Name     string   `yaml:"Name"`
	Commands []string `yaml:"Commands"`
	Skip     bool     `yaml:"Skip"`
	Count    int      `yaml:"Count"`
}

func Init() error {

	wd, _ := os.Getwd()
	log.Infof("wd:%v", wd)

	cfg, err := LoadConfig("./config/config.yaml")
	if err != nil {
		return err
	}
	globalCfg = cfg
	return nil
}

func GetConfig() *GlobalConfig {
	return globalCfg
}

func (g *GlobalConfig) Print() {
	resp, _ := json.MarshalIndent(g, "", " ")
	log.Infof("cfg:%v", string(resp))
}

func LoadConfig(path string) (*GlobalConfig, error) {

	cfgData, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	cfg := &GlobalConfig{}
	err = yaml.Unmarshal(cfgData, cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil

}
