package config

import "auto_task.com/base/action"

type ShiLian struct {
	Items []ShiLianItem
}

type ShiLianItem struct {
}

type Pos struct {
	Name    string
	X       int
	Y       int
	WaitSec int
	Toggle  string //x1,y1>x2,y2
}

func ParsePos(req *Pos) *action.Pos {
	resp := action.NewPos(req.Name, req.X, req.Y).WithWait(req.WaitSec)
	return resp
}
