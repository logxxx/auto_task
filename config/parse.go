package config

import (
	"auto_task.com/base/action"
	"errors"
	"strconv"
	"strings"
)

func ParseCommands(commands []string) ([]action.Action, error) {

	actions := make([]action.Action, 0)
	for _, c := range commands {
		act, err := ParseCommand(c)
		if err != nil {
			return nil, err
		}
		actions = append(actions, act)
	}

	return actions, nil

}

func ParseCommand(command string) (action.Action, error) {

	elems := strings.Split(command, ":")
	if len(elems) != 2 {
		return nil, errInvalidCommand
	}

	action := elems[0]
	content := elems[1]

	switch action {
	case ActionClick:
		return ParseClick(content)
	case ActionToggle:
		return ParseToggle(content)
	case ActionWait:
		return ParseWait(content)
	default:
		return nil, errInvalidCommand
	}

	return nil, errInvalidCommand

}

func ParseWait(content string) (*action.WaitAction, error) {
	return action.NewWaitDuration(content)
}

func ParseToggle(content string) (*action.ToggleAction, error) {

	elems := strings.Split(content, "->")
	if len(elems) != 2 {
		return nil, errors.New("split > failed")
	}

	src := elems[0]
	dst := elems[1]

	srcX, srcY, err := splitPos(src)
	if err != nil {
		return nil, err
	}

	dstX, dstY, err := splitPos(dst)
	if err != nil {
		return nil, err
	}

	t := action.NewToggle(srcX, srcY, dstX, dstY)

	return t, nil
}

func ParseClick(content string) (*action.ClickAction, error) {

	elems := strings.Split(content, ",")
	if len(elems) != 2 {
		return nil, errors.New("split , failed")
	}

	x := 0
	y := 0
	count := 0

	xStr := elems[0]
	yStr := elems[1]
	countStr := "1"
	addIdx := strings.Index(yStr, "+")
	if addIdx > -1 {
		countStr = yStr[addIdx:]
		yStr = yStr[:addIdx]
	}

	x, err := strconv.Atoi(xStr)
	if err != nil {
		return nil, err
	}

	y, err = strconv.Atoi(yStr)
	if err != nil {
		return nil, err
	}

	count, err = strconv.Atoi(countStr)
	if err != nil {
		return nil, err
	}

	return action.NewClick(x, y).SetCount(count), nil

}
