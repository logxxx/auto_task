package config

import (
	"errors"
	"strconv"
	"strings"
)

// 分解坐标
// input: 123,456
// output: 123 456
func splitPos(input string) (int, int, error) {
	elems := strings.Split(input, ",")
	if len(elems) != 2 {
		return 0, 0, errors.New("split , failed")
	}

	e1, err := strconv.Atoi(elems[0])
	if err != nil {
		return 0, 0, err
	}

	e2, err := strconv.Atoi(elems[1])
	if err != nil {
		return 0, 0, err
	}

	return e1, e2, nil

}
