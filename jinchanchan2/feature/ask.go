package feature

import (
	"auto_task.com/base/screen"
	"auto_task.com/base/utils/fileutil"
	"encoding/json"
	"image"
	"io/ioutil"
	"sync"
)

var (
	loadLock   sync.Mutex
	featureLib []*Feature
)

type AskResult struct {
	Name  string
	Score int
}

func CaptureAndAsk() ([]*AskResult, error) {

	imgs, err := captureAll()
	if err != nil {
		return nil, err
	}

	results := make([]*AskResult, 0)
	for _, img := range imgs {
		result, err := Ask(img)
		if err != nil {
			return nil, err
		}
		results = append(results, result)
	}

	return results, nil

}

func Ask(img *image.RGBA) (*AskResult, error) {

	featureLib := getFeatureLib()

	imgFeature, err := calFeatureCore(img)
	if err != nil {
		return nil, err
	}

	result := &AskResult{
		Score: 9999999,
	}
	for _, f := range featureLib {
		score, err := compareCore(imgFeature, f)
		if err != nil {
			return nil, err
		}
		if result.Score > score {
			result.Score = score
			result.Name = f.Name
		}
	}

	if result.Score > 1000 {
		result.Name = "未知"
	}

	return result, nil

}

func getFeatureLib() []*Feature {

	if featureLib != nil {
		return featureLib
	}

	loadLock.Lock()
	defer loadLock.Unlock()

	if featureLib != nil {
		return featureLib
	}

	fileName := "./lib/feature.json"
	if !fileutil.HasFile(fileName) {
		panic("feature.json not exist")
	}
	featureLibJson, _ := ioutil.ReadFile(fileName)
	if len(featureLibJson) == 0 {
		panic("feature.json empty")
	}

	tmp := make([]*Feature, 0)

	err := json.Unmarshal(featureLibJson, &tmp)
	if err != nil {
		panic(err)
	}

	featureLib = tmp

	return featureLib
}

func captureAll() ([]*image.RGBA, error) {

	leftTopX := 318
	leftTopY := 650
	cardWidth := 129
	cardHeight := 94
	jiange := 5

	imgs := make([]*image.RGBA, 0)
	for i := 0; i < 5; i++ {
		leftTopXNow := leftTopX + i*(cardWidth+jiange)
		rightButtomX := leftTopXNow + cardWidth
		rightButtomY := leftTopY + cardHeight

		x1 := leftTopXNow
		y1 := leftTopY
		x2 := rightButtomX
		y2 := rightButtomY

		img, err := screen.ShotRectOrig(x1, y1, x2, y2)
		if err != nil {
			return nil, err
		}

		imgs = append(imgs, img)

	}

	return imgs, nil
}
