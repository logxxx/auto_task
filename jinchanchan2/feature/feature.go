package feature

import (
	"auto_task.com/base/screen"
	"auto_task.com/base/utils"
	"auto_task.com/base/utils/fileutil"
	"auto_task.com/base/utils/log"

	"encoding/json"
	"fmt"
	"image"
	"io/ioutil"
)

type Feature struct {
	Name     string
	Desc     string
	TotalNum int //总的像素点个素数
	Items    []FeatureItem
}

type FeatureItem struct {
	Rank int
	RNum int
	GNum int
	BNum int
}

func Learn() error {
	leftTopX := 318
	leftTopY := 650
	cardWidth := 129
	cardHeight := 94
	jiange := 5
	features := make([]*Feature, 0)

	libFileIdx, libFileName := getLibFileIdxAndName()

	needs := []int{0, 3}
	for i := 0; i < 5; i++ {
		if !utils.IsInSlice(i, needs) {
			log.Infof("%v not need", i)
			continue
		}
		leftTopXNow := leftTopX + i*(cardWidth+jiange)
		rightButtomX := leftTopXNow + cardWidth
		rightButtomY := leftTopY + cardHeight
		feature, err := CalFeature(libFileIdx, i, leftTopXNow, leftTopY, rightButtomX, rightButtomY)
		if err != nil {
			return err
		}
		features = append(features, feature)

	}

	err := SaveFeatures(libFileName, features)
	if err != nil {
		return err
	}

	return nil
}

func CalFeature(libFileIdx, cardIdx int, x1, y1, x2, y2 int) (*Feature, error) {

	img, err := screen.ShotRectOrig(x1, y1, x2, y2)
	if err != nil {
		return nil, err
	}

	log.Infof("ShotRectOrig w:%v h:%v", 445-318, 744-650)

	filename := fmt.Sprintf("./asset/%v_%v.jpg", libFileIdx, cardIdx)
	err = screen.SaveToLocal(img, filename)
	if err != nil {
		return nil, err
	}

	feature, err := calFeatureCore(img)
	if err != nil {
		return nil, err
	}

	feature.Name = fmt.Sprintf("%v_%v", libFileIdx, cardIdx)
	feature.Desc = filename

	return feature, nil
}

func calFeatureCore(img *image.RGBA) (*Feature, error) {

	log.Infof("bound x:%v y:%v", img.Bounds().Dx(), img.Bounds().Dy())

	rMap := make(map[uint8]int)
	gMap := make(map[uint8]int)
	bMap := make(map[uint8]int)

	for i := 0; i < img.Bounds().Dx(); i++ {
		for j := 0; j < img.Bounds().Dy(); j++ {
			r, g, b, _ := img.At(i, j+2).RGBA()
			rMap[uint8(r)/5*5]++
			rMap[uint8(g)/5*5]++
			rMap[uint8(b)/5*5]++
		}
	}

	feature := &Feature{
		TotalNum: img.Bounds().Dx() * img.Bounds().Dy(),
	}
	for i := 0; i <= 255; i += 5 {
		item := FeatureItem{
			Rank: i,
			RNum: rMap[uint8(i)],
			GNum: gMap[uint8(i)],
			BNum: bMap[uint8(i)],
		}
		feature.Items = append(feature.Items, item)
	}

	return feature, nil
}

func getLibFileIdxAndName() (int, string) {
	libFileName := ""
	i := 1
	for {
		libFileName = fmt.Sprintf("./lib/feature_%v.json", i)
		if exists := fileutil.HasFile(libFileName); !exists {
			break
		}
		i++
	}

	return i, libFileName
}

func SaveFeatures(libFileName string, features []*Feature) error {

	featureData, _ := ioutil.ReadFile(libFileName)
	if len(featureData) == 0 {
		featureData = []byte("{}")
	}

	featureJson, _ := json.MarshalIndent(features, "", " ")

	err := ioutil.WriteFile(libFileName, featureJson, 0777)
	if err != nil {
		return err
	}

	return nil
}

// 归档
func Merge() error {
	features, err := readFromFile()
	if err != nil {
		return err
	}

	merged := make([]*Feature, 0)
	mergedNames := make(map[string]bool, 0)
	for _, f := range features {
		f.Name = f.Name[3:]
		if f.Name == "" {
			continue
		}
		if mergedNames[f.Name] {
			continue
		}
		mergedNames[f.Name] = true

		log.Printf("merge:%v", f.Name)
		merged = append(merged, f)
	}

	SaveFeatures("./lib/feature.json", features)

	return nil
}

func readFromFile() ([]*Feature, error) {
	features := make([]*Feature, 0)
	i := 0
	for {
		i++
		fileName := fmt.Sprintf("./lib/feature_%v.json", i)
		if !fileutil.HasFile(fileName) {
			break
		}
		featureLibJson, _ := ioutil.ReadFile(fileName)
		if len(featureLibJson) == 0 {
			panic(i)
		}

		part := make([]*Feature, 0)

		err := json.Unmarshal(featureLibJson, &part)
		if err != nil {
			panic(err)
		}

		features = append(features, part...)

	}

	log.Infof("共读入%v个特征。", len(features))

	return features, nil
}

func Compare() error {

	features, err := readFromFile()
	if err != nil {
		return err
	}

	for i := range features {
		for j := range features {
			if j <= i {
				continue
			}
			score, err := compareCore(features[i], features[j])
			if err != nil {
				panic(err)
			}

			same := ""
			if features[i].Name[3:] == features[j].Name[3:] {
				same = "[SAME]"
			}
			log.Printf("%v[%v]VS[%v] %v VS %v score:%v", same, i, j, features[i].Name, features[j].Name, score)
			log.Printf("desc:%v", features[i].Desc)
			log.Printf("desc:%v", features[j].Desc)
		}
	}

	return nil
}

func compareCore(fA, fB *Feature) (int, error) {

	type DiffResult struct {
		Rank   int
		Num    int
		Detail string
	}

	results := make([]*DiffResult, 0)
	totalDiffNum := 0
	for i := range fA.Items {

		result := &DiffResult{
			Rank: fA.Items[i].Rank,
		}

		rDiff := utils.Abs(fA.Items[i].RNum - fB.Items[i].RNum)
		gDiff := utils.Abs(fA.Items[i].GNum - fB.Items[i].GNum)
		bDiff := utils.Abs(fA.Items[i].BNum - fB.Items[i].BNum)

		result.Num = rDiff + gDiff + bDiff
		result.Detail = fmt.Sprintf("r:%v g:%v b:%v", rDiff, gDiff, bDiff)
		results = append(results, result)

		totalDiffNum += result.Num

		//log.Infof("diffResult:%+v", result)

	}

	log.Printf("totalDiffNum:%+v ratio:%v/%v", totalDiffNum, totalDiffNum, fA.TotalNum)

	return totalDiffNum * 10000000 / fA.TotalNum / 10000, nil

}
