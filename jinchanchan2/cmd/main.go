package main

import (
	"auto_task.com/base/utils/fileutil"
	"auto_task.com/base/utils/log"
	"auto_task.com/jinchanchan2/feature"
)

var (
	LearnOrCompare = true
)

func main() {

	result, err := feature.CaptureAndAsk()
	if err != nil {
		panic(err)
	}

	for i, res := range result {
		log.Infof("%v %v(%v)", i, res.Name, res.Score)
	}

	return

	feature.Merge()
	return

	//LearnOrCompare = false

	if LearnOrCompare {
		//clean()
		err := feature.Learn()
		if err != nil {
			panic(err)
		}
	} else {
		err := feature.Compare()
		if err != nil {
			panic(err)
		}
	}

}

func clean() {
	fileutil.CleanDir("asset")
	fileutil.CleanDir("lib")
}
