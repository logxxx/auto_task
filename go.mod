module auto_task.com

go 1.14

require (
	github.com/blend/go-sdk v1.20220411.3 // indirect
	github.com/garyburd/redigo v1.6.3 // indirect
	github.com/go-vgo/robotgo v0.100.10
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/kbinani/screenshot v0.0.0-20210720154843-7d3a670d8329
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
	github.com/otiai10/mint v1.3.2 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common v1.0.370
	github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/ocr v1.0.370
	github.com/wcharczuk/go-chart v2.0.1+incompatible
	gopkg.in/bsm/ratelimit.v1 v1.0.0-20160220154919-db14e161995a // indirect
	gopkg.in/redis.v3 v3.6.4
	gopkg.in/yaml.v2 v2.4.0
)
