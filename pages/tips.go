package pages

import "auto_task.com/base/action"

//tips:存放一些小动作。

var (
	Mail = action.NewPos("邮件", 887, 147)
)

// 清理背包
// 包含:自动领邮件，和合并,和卖无用的装备
func CleanBag() {
	var (
		RecvAll = action.NewPos("一键领取", 845, 631)
		LinShi  = action.NewPos("侧边栏_临时", 1111, 365)
		Close   = action.NewPos("关闭", 1100, 141)

		Bag_HeBing         = action.NewPos("背包_一键合并", 884, 681)
		Bag_HeBing_Commit  = action.NewPos("背包_一键合并_确认", 764, 520)
		Bag_HeBing_Commit2 = action.NewPos("背包_一键合并_二次确认", 748, 473)

		Bag_Sell           = action.NewPos("背包_批量出售", 1056, 681)
		Bag_Sell_ChooseAll = action.NewPos("背包_批量出售_全选", 520, 640)
		Bag_Sell_ChooseLow = action.NewPos("背包_批量出售_选中蓝色及以下", 191, 643)
		Bag_Sell_Sell      = action.NewPos("背包_批量出售_出售", 1006, 642)

		Bag_Diamond = action.NewPos("批量出售_宝石", 427, 169)
		Bag_QiGong  = action.NewPos("批量出售_气功", 594, 171)
	)
	action.ClickPoss(
		Mail,
		RecvAll,
		LinShi,
		RecvAll,
		Close,
	)
	action.ClickPoss(
		Bag,
		Bag_HeBing,
		Bag_HeBing_Commit,
		Bag_HeBing_Commit2,
	//合并完 退回到背包界面
	)

	//批量出售装备
	action.ClickPoss(
		Bag_Sell,
		Bag_Sell_ChooseAll,
		Bag_Sell_Sell,
		action.WaitPos("等待出售完", 1),
	)

	//批量出售宝石
	action.ClickPoss(
		Bag_Diamond,
		Bag_Sell_ChooseLow,
		Bag_Sell_Sell,
		action.WaitPos("等待出售完", 1),
	)

	//批量出售气功
	action.ClickPoss(
		Bag_QiGong,
		Bag_Sell_Sell,
		action.WaitPos("等待出售完", 1),
	)

	action.ClickPoss(
		Close,
		Bag_Close,
	)
}

// 购买铜钱
func BuyTongQian() {

	var Enter = action.NewPos("进入铜钱", 812, 68)
	var BuyButton = action.NewPos("连续购买", 760, 416)
	var Commit = action.NewPos("确认", 748, 466)
	var Quit = action.NewPos("退出铜钱", 885, 175)

	action.ClickPoss(
		Detail,
		Enter,
		BuyButton,
		Commit,
		Quit,
		Detail_Close)
}
