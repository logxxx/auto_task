package pages

import (
	"auto_task.com/base/action"
	"auto_task.com/base/utils/color"
	"auto_task.com/base/utils/log"
	"time"
)

var (
	Faction_Enter              = action.NewPos("进入帮派", 788, 696)
	Faction_Close              = action.NewPos("退出帮派", 1239, 62)
	Faction_ZhengTaoTang       = action.NewPos("帮派_征讨堂", 722, 213)
	Faction_LingXiaoGe         = action.NewPos("帮派_凌霄阁", 952, 316)
	Faction_ZhengTaoTang_Close = CommonClosePage
)

func Faction() {
	intoFaction()
	defer leaveFaction()

	FinishFactionTask()
	LingXiaoGe()
}

func intoFaction() {
	action.ClickPoss(Detail, Faction_Enter)
}

func leaveFaction() {
	action.ClickPoss(Faction_Close, Detail_Close)
}

//完成帮派任务
func FinishFactionTask() {

	log.Title("帮派任务 开始")
	log.Title("帮派任务 结束")

	OtherShare := action.NewPos("他人共享", 385, 152)
	MyTasks := action.NewPos("任务库", 229, 157)

	FinishQuickly := action.NewPos("快速完成", 962, 229)

	Blank := action.NewPos("空白", 626, 600)

	action.ClickPoss(Faction_ZhengTaoTang, OtherShare)

	//直接完成别人共享的任务
	for i := 0; i < 6; i++ {
		action.ClickPoss(FinishQuickly, Blank)
	}

	//切到我的任务库，防止他人共享的不够
	action.ClickPos(MyTasks)

	for i := 0; i < 6; i++ {
		action.ClickPoss(FinishQuickly, Blank)
	}

	action.ClickPoss(Faction_ZhengTaoTang_Close)

}

//TODO:凌霄阁
func LingXiaoGe() {

	log.Title("凌霄阁 开始")
	defer log.Title("凌霄阁 结束")
	//f1 := NewPos("凌霄阁_一层", 291, 245)
	//f2 := NewPos("凌霄阁_二层", 291, 325)
	f4 := action.NewPos("凌霄阁_四层", 291, 505)
	toggle := action.PosToggle("五层->一层的位置", 305, 593, 305, 230)

	enter := action.NewPos("进入副本", 1009, 549)
	enter2 := action.NewPos("进入副本2", 946, 577)
	leave := action.NewPos("离开", 814, 74)
	leave_Commit := action.NewPos("离开_确认", 748, 465)

	close := action.NewPos("关闭", 1140, 117)

	action.ClickPoss(Faction_LingXiaoGe)

	//八层=toggle+4层
	for i := 0; i < 2; i++ {
		action.ClickPoss(toggle,
			f4,
			enter,
			enter2,
			action.WaitPos("待几秒", 8),
			leave,
			leave_Commit,
		)
	}

	action.ClickPoss(close)
}

// 帮派运镖
var (
	BiaoJu           = action.NewPos("镖局", 377, 581)
	Biao1            = action.NewPos("镖1", 528, 577)
	Biao2            = action.NewPos("镖2", 747, 577)
	YunBiao_Continue = action.NewPos("继续", 754, 469)
	YunBiao_Commit   = action.NewPos("运镖", 882, 652)
)

func YunBiao(round int) {

	var Biao *action.Pos
	if round == 1 {
		Biao = Biao1
	} else {
		Biao = Biao2
	}
	intoFaction()
	//领任务
	action.ClickPoss(
		BiaoJu,
		Biao,
		YunBiao_Continue,
		YunBiao_Commit,
		action.WaitPos("loading", 5),
	)

	//开始运镖
	var (
		FindCar = action.NewPos("寻车", 372, 602)
		FindWay = action.NewPos("寻劲", 602, 602)
	)

	count := 0
	//7分钟完成。
	for {

		ok := color.CheckColor(386, 616, 591, 660, false, color.IsGreen)
		if ok {
			log.Infof("运镖到站了!!!")
			action.NewPos("运镖结束", 495, 636).Click()
			action.NewPos("空白", 581, 554).Click()
			return
		}

		action.ClickPos(FindWay)
		count++
		if count%5 == 0 {
			action.ClickPos(FindCar)
			time.Sleep(2 * time.Second)
			continue
		}
		time.Sleep(5 * time.Second)
	}
}
