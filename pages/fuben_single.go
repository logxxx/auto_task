package pages

import (
	"auto_task.com/base/action"
	"auto_task.com/base/utils/log"
	"time"
)

var (
	SingleFuben_LONGTINGYA     = NewFubenPos(nil, action.NewPos("龙庭崖", 244, 209))
	SingleFuben_XIEJIAOMIKU    = NewFubenPos(nil, action.NewPos("邪教米库", 255, 348))
	SingleFuben_BISHILIN       = NewFubenPos(nil, action.NewPos("鄙视林", 255, 488))
	SingleFuben_HUXUEBIEYUAN   = NewFubenPos(nil, action.NewPos("虎穴别院", 255, 638))
	SingleFuben_BEIHAIDONGDING = NewFubenPos(toggle, action.NewPos("北海洞顶", 255, 209))
	SingleFuben_XIXIESHENGDIAN = NewFubenPos(toggle, action.NewPos("西邪圣殿", 255, 348))
	SingleFuben_SHENGONG       = NewFubenPos(toggle, action.NewPos("神宫", 255, 348))
	SingleFuben_SHANXISI       = NewFubenPos(func() { toggle(); toggle() }, action.NewPos("山夕寺", 255, 348))

	Choose_Simple    = action.NewPos("选择普通模式", 603, 557)
	Choose_Difficult = action.NewPos("选择困难模式", 971, 556)

	SingleFuben_Items = []*FubenPos{
		//SingleFuben_LONGTINGYA,
		SingleFuben_XIEJIAOMIKU,
		SingleFuben_BISHILIN,
		SingleFuben_HUXUEBIEYUAN,
		//SingleFuben_BEIHAIDONGDING,
		//SingleFuben_XIXIESHENGDIAN,
		//SingleFuben_SHENGONG,
		//SingleFuben_SHANXISI,
	}
)

//单人副本
func RunSingleFuBen() {

	st := time.Now()
	defer func() {
		used := time.Now().Sub(st)
		usedSec := int64(used.Seconds())
		log.Infof("完成[%v]花费了%v分%v秒.", "单人副本", usedSec/60, usedSec%60)
	}()

	for _, pos := range SingleFuben_Items {
		for i := 0; i < 2; i++ {
			action.ClickPos(Detail)
			action.ClickPos(FuBen)
			if pos.Toggle != nil {
				pos.Toggle()
			}
			action.ClickPos(pos.Pos)
			action.ClickPos(Choose_Difficult)
			action.ClickPos(KuaiSuJiaRu)

			commonWaitToQuit()
		}
	}
}
