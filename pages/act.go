package pages

import "auto_task.com/base/action"

var (
	Act       = action.NewPos("活动", 746, 66)
	Act_Close = CommonClosePage
)

// 领取活跃度奖励
func RecvHuoYueDuBonus() {

	Blank := action.NewPos("空白", 948, 93)
	DailyBonus1 := action.NewPos("日常_奖励1", 462, 215)
	DailyBonus2 := action.NewPos("日常_奖励2", 617, 215)
	DailyBonus3 := action.NewPos("日常_奖励3", 763, 215)
	DailyBonus4 := action.NewPos("日常_奖励4", 906, 215)
	DailyBonus5 := action.NewPos("日常_奖励5", 1027, 215)

	WeeklyBonus1 := action.NewPos("周常_奖励1", 413, 223)
	WeeklyBonus2 := action.NewPos("周常_奖励2", 568, 223)
	WeeklyBonus3 := action.NewPos("周常_奖励3", 713, 223)
	WeeklyBonus4 := action.NewPos("周常_奖励4", 863, 223)
	WeeklyBonus5 := action.NewPos("周常_奖励5", 1012, 223)

	SidBar_DailyBonus := action.NewPos("侧边栏_日常", 1118, 482)
	SidBar_WeeklyBonus := action.NewPos("侧边栏_周常", 1116, 603)
	SidBar_DailyBonus_Recv := action.NewPos("日常_领取", 976, 171)
	SidBar_WeeklyBonus_Recv := action.NewPos("周常_领取", 963, 355)

	action.ClickPos(Act)

	for i := 0; i < 2; i++ {
		for _, pos := range []*action.Pos{
			DailyBonus1,
			DailyBonus2,
			DailyBonus3,
			DailyBonus4,
			DailyBonus5} {
			action.ClickPos(pos)
			action.ClickPos(Blank)
		}
	}

	//领取日常奖励
	action.ClickPoss(
		SidBar_DailyBonus,
		SidBar_DailyBonus_Recv,
		Blank,
	)

	//周常
	action.ClickPoss(
		SidBar_WeeklyBonus,
		SidBar_WeeklyBonus_Recv,
		Blank,
	)

	//周常的积分奖励
	for _, pos := range []*action.Pos{
		WeeklyBonus1,
		WeeklyBonus2,
		WeeklyBonus3,
		WeeklyBonus4,
		WeeklyBonus5,
	} {
		action.ClickPoss(pos, Blank)
	}

	action.ClickPos(Act_Close)

}
