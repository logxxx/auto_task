package pages

import (
	"auto_task.com/base/action"
	"auto_task.com/base/mouse"

	"auto_task.com/base/utils"
	"auto_task.com/base/utils/log"
	"context"
	"fmt"
	"time"
)

var (
	JiaYuan              = action.NewPos("家园", 430, 600)
	JiaYuan_Leave        = action.NewPos("家园_离开", 823, 77)
	JiaYuan_Leave_Commit = action.NewPos("家园_离开_确认", 755, 470)
	Items_Open           = action.NewPos("展开选项", 970, 71)
	Bag                  = action.NewPos("背包", 970, 145)
	Bag_Close            = CommonClosePage
	Bag_Filter           = action.NewPos("背包_筛选", 1115, 159)
	Bag_FirstItem        = action.NewPos("背包_第一格", 676, 249)
	BagItem_Use          = action.NewPos("背包物品_使用", 853, 436)

	JiaYuan_Equip = action.NewPos("家园装备", 1109, 475)

	NpcList       = action.NewPos("Npc列表", 1222, 73)
	NpcList_Close = action.NewPos("关闭", 1103, 163)
)

var (
	isWaitingForHervest bool
)

func CleanFarmCounter() {
	setFarmtoolHealth(0)
	setPlantHarvestCount(0)
	setPlantHervestTime(time.Time{})
}

func Farming(ctx context.Context) {

	//CleanFarmCounter()

	hervestTime := getPlantHervestTime()
	log.Infof("果实收获时间:%v", utils.Format(hervestTime))
	if !hervestTime.IsZero() { //可以等待收获
		if isWaitingForHervest { //已经在等待收获了
			return
		}
		go utils.SleepAndDo(ctx, "摘果子", hervestTime.Sub(time.Now()), recvFruit)
		isWaitingForHervest = true
		return
	}

	intoHomeland()

	//走到田里
	log.Title("走到田里")
	action.ClickPoss(
		NpcList,
		action.NewPos("田地", 368, 461),
		action.WaitPos("赶路ing", 3),
		NpcList_Close,
	)

	//买种子
	log.Title("买种子")
	action.ClickPoss(
		action.NewPos("点击田地", 782, 404),
		action.NewPos("家园商人", 917, 548),
		//买太阳菊种子
		action.ToggleY(365, 505, 267),
		action.NewPos("太阳菊", 631, 487),
		action.NewPos("确定", 742, 579),
	)

	health := getFarmtoolHealth()
	log.Infof("锄头健康度:%v", health)
	if health <= 0 {
		log.Title("买锄头")
		//买锄头
		action.ClickPoss(
			action.NewPos("锄头", 360, 285), //有种子toggle
			action.NewPos("确定", 742, 579),
		)
		setFarmtoolHealth(100)
	}

	//关闭
	action.ClickPoss(
		NpcList_Close,
		action.NewPos("关闭", 1032, 142),
	)
	if health <= 0 {
		log.Title("消耗锄头")
		//消耗锄头
		farmTool_Consume()
	}

	//装备锄头
	log.Title("装备锄头")
	farmTool_Equip()

	//种植
	log.Title("种植")
	action.ClickPoss(
		action.NewPos("点击田地", 782, 404),
		action.NewPos("点击菊花种子", 325, 293),
		action.NewPos("种植", 766, 549),
		action.NewPos("确定", 757, 466),
	)
	setPlantHarvestCount(3)

	//60分钟后来收菜
	setPlantHervestTime(time.Now().Add(60 * time.Minute))
	go utils.SleepAndDo(ctx, "第1次摘果子", 60*time.Minute, recvFruit)

	//种植一次，消耗1点耐久度。可以种100次再买
	consumeFarmtool()

	leaveHomeland()

}

func recvFruit(ctx context.Context) {

	resp := updatePlantHarvestCount(-1)
	if resp <= 0 {
		log.Infof("recvFruit 果子收完了，重新种植。")
		Farming(ctx)
		return
	}

	log.Infof("recvFruit 开始收果子。")

	intoHomeland()

	//走到田里
	action.ClickPoss(
		NpcList,
		action.NewPos("田地", 368, 461),
		action.WaitPos("赶路ing", 3),
		NpcList_Close,
		action.NewPos("收获", 524, 498),
		action.NewPos("空白", 524, 498),
	)

	//首次收获后，只需30分钟，就熟了
	setPlantHervestTime(time.Now().Add(30 * time.Minute))
	go utils.SleepAndDo(ctx, fmt.Sprintf("第%v次摘果子", 3-resp), 30*time.Minute, recvFruit)

	leaveHomeland()
}

func Fishing() {
	intoHomeland()
	fishTool_Buy()
	fishTool_Consume()
	fishTool_Equip()
	goFishing()
	leaveHomeland()
}

func intoHomeland() {
	log.Title("进入家园")
	//去一个安全的地方，避免战斗状态
	action.ClickPoss(
		Map,
		action.NewPos("杂货店", 1042, 260),
		action.WaitPos("loading下", 2),
	)

	Enter := action.NewPos("进入家园", 955, 620)
	action.ClickPoss(
		Detail,
		JiaYuan,
		action.WaitPos("loading", 1),
		Enter,
		action.WaitPos("loading", 3),
	)
}

func leaveHomeland() {
	action.ClickPoss(
		JiaYuan_Leave,
		JiaYuan_Leave_Commit,
		TuoGuan)
}

func fishTool_Buy() {

	NpcShop := action.NewPos("杂货", 469, 394)
	NpcShop_Close := NpcList_Close
	NpcShop_Commit := action.NewPos("确定", 739, 582)
	//Add1 := NewPos("+1", 680, 440)
	//ZhuangBei := NewPos("家园装备", 1109, 476)
	action.ClickPoss(
		NpcList,
		NpcShop,
		NpcList_Close,
		action.WaitPos("跑到杂货商那里", 30),
		action.NewPos("杂货本人", 669, 362),
		action.NewPos("货郎按钮", 495, 638),
		//买5根鱼竿
		//todo: bug: 点了+1不管用
		action.NewPos("普通鱼竿", 364, 295), NpcShop_Commit,
		action.NewPos("普通鱼竿", 364, 295), NpcShop_Commit,
		action.NewPos("普通鱼竿", 364, 295), NpcShop_Commit,
		action.NewPos("普通鱼竿", 364, 295), NpcShop_Commit,
		action.NewPos("普通鱼竿", 364, 295), NpcShop_Commit,
		//买5个鱼饵
		action.NewPos("鱼饵", 364, 648), NpcShop_Commit,
		action.NewPos("鱼饵", 364, 648), NpcShop_Commit,
		action.NewPos("鱼饵", 364, 648), NpcShop_Commit,
		action.NewPos("鱼饵", 364, 648), NpcShop_Commit,
		action.NewPos("鱼饵", 364, 648), NpcShop_Commit,
		NpcShop_Close,
	)

}

func farmTool_Consume() {
	tool_Consume(false)
}

func tool_Consume(isFishTool bool) {
	action.ClickPoss(
		Items_Open,
		Bag,
		Bag_Filter,
	)

	//消耗鱼竿
	//神兵拉到第一个
	mouse.Toggle(1049, 399, 1049, 211)
	//药品拉到第一个
	mouse.Toggle(1049, 399, 1049, 211)
	action.ClickPoss(
		action.NewPos("背包筛选_家园", 1049, 362),
	)

	consumeCount := 1 //一铲子
	if isFishTool {
		consumeCount = 10 //5鱼竿+5鱼饵
	}
	for i := 0; i < consumeCount; i++ {
		action.ClickPoss(Bag_FirstItem, BagItem_Use)
	}

	action.ClickPoss(
		Bag_Close,
	)
}

func fishTool_Consume() {
	tool_Consume(true)
}

func tool_Equip(isFishTool bool) {
	ZhuangBei := action.NewPos("装备", 866, 471)
	action.ClickPoss(
		Detail,
		JiaYuan,
		JiaYuan_Equip,
		//装备鱼饵
		Bag_FirstItem, ZhuangBei,
		//装备鱼竿
		action.NewPos("家园装备_筛选", 1041, 178),
		action.NewPos("家园装备_筛选_武器", 971, 290),
		Bag_FirstItem, ZhuangBei,
	)
	//因为锄头的耐久度消耗太慢。所以在第一个。
	//(即使先买鱼竿再买锄头，锄头也在第一个。)
	//装备了锄头后，鱼竿在第一个。
	//所以再装备一次，就是鱼竿了。
	//(就算只有鱼竿，这样做也是幂等的。)
	//(只要保证工具栏里，最多只有1把锄头。)
	if isFishTool {
		action.ClickPoss(Bag_FirstItem, ZhuangBei)
	}
	action.ClickPoss(
		action.NewPos("家园装备_关闭", 1106, 135),
		Detail_Close,
	)
}

func fishTool_Equip() {
	tool_Equip(true)
}

func farmTool_Equip() {
	tool_Equip(false)
}

func goFishing() {
	Fishing_TuoGuan := action.NewPos("钓鱼_托管", 1207, 280)
	Fishing_TuoGuan_Commit := action.NewPos("钓鱼_托管_确定", 755, 466)
	action.ClickPoss(
		NpcList,
		action.NewPos("钓鱼精确位置", 508, 456),
		action.WaitPos("寻路", 30),
		NpcList_Close,
		action.NewPos("点地", 529, 472),
		action.NewPos("钓鱼按钮", 638, 442),
		Fishing_TuoGuan,
		Fishing_TuoGuan_Commit,
	)

	//41:20开始
	//50:20一半 化9分钟
	//基本10s一条鱼。100条要1000s
	fangdiaoxian := func() { //防掉线，定期点击话筒
		action.ClickPos(action.NewPos("话筒", 750, 671))
	}
	utils.TickWithFunc(nil, 5*1000*time.Second, fangdiaoxian)
}
