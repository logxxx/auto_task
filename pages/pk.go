package pages

import (
	"auto_task.com/base/action"
	"auto_task.com/base/utils"
	"auto_task.com/base/utils/log"
	"time"
)

var (
	JinJi                      = action.NewPos("竞技", 105, 611)
	JinJi_YiJianSaoDang        = action.NewPos("竞技_一键扫荡", 1064, 676)
	JinJi_Add5                 = action.NewPos("竞技_+5", 784, 314)
	JinJi_YiJianSaoDang_QueRen = action.NewPos("竞技_一键扫荡_确认", 749, 498)
	JinJi_Close                = action.NewPos("竞技_关闭", 1174, 115)

	RankFirst      = action.NewPos("pk榜第一个人", 957, 246)
	JinJi_TiaoZhan = action.NewPos("挑战", 637, 547)
	JinJi_Pet1     = action.NewPos("竞技_宠物1", 267, 411)
	JinJi_Pet2     = action.NewPos("竞技_宠物2", 419, 411)
	JinJi_Pet3     = action.NewPos("竞技_宠物3", 578, 411)
	JinJi_KaiZhan  = action.NewPos("竞技_开战", 1040, 501)
)

func PK(saodang bool) {
	if saodang {
		PKBySaoDang()
	} else {
		PKByFight()
	}
}

func PKByFight() {
	count := 5
	for i := 0; i < count; i++ {
		log.Infof("第%v/%v次pk", i+1, count)
		action.ClickPos(Detail)
		action.ClickPos(JinJi)
		action.ClickPos(RankFirst)
		action.ClickPos(JinJi_TiaoZhan)
		if i == 0 {
			//上宠物
			action.ClickPos(JinJi_Pet1)
			action.ClickPos(JinJi_Pet2)
			action.ClickPos(JinJi_Pet3)
		}

		action.ClickPos(JinJi_KaiZhan)

		//loading 5s
		//ready 5s
		//pk 10s
		//result 20s
		utils.SleepSec(40)
		//TODO 考虑做成ask machine 快速结束

		action.ClickPos(JinJi_Close)
		action.ClickPos(Detail_Close)
		//不影响外部的托管状态，所以不用点托管
	}

}

//扫荡方式完成pk
func PKBySaoDang() {
	action.ClickPos(Detail)
	action.ClickPos(JinJi)
	action.ClickPos(JinJi_YiJianSaoDang)
	//ClickPos(JinJi_Add5)
	action.ClickPos(JinJi_YiJianSaoDang_QueRen)
	time.Sleep(3 * time.Second)
	action.ClickPos(JinJi_Close) //点击空白退出结算
	action.ClickPos(JinJi_Close)
	action.ClickPos(Detail_Close)
	//ClickPos(TuoGuan)
	//ClickPos(QiCheng)
}
