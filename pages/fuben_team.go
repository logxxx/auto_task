package pages

import (
	"auto_task.com/base/action"
	"auto_task.com/base/mouse"
	"fmt"

	"auto_task.com/base/utils/log"
	"time"
)

var (
	JIAOZHUNPOINT_CHUBEI_CENTER   = action.NewPos("校准。【储备】的中心点", 398, 91)
	JIAOZHUNPOINT_MIAOZHUN_CENTER = action.NewPos("校准。【瞄准】的中心点", 1184, 370)
	FuBen                         = action.NewPos("副本", 100, 457)
	TeamFuBen_XIEJIAOMIKU         = NewFubenPos(nil, action.NewPos("邪教米库", 255, 203))
	TeamFuBen_BISHILIN            = NewFubenPos(nil, action.NewPos("鄙视林", 255, 348))
	TeamFuBen_HUXUEBIEYUAN        = NewFubenPos(nil, action.NewPos("虎穴别院", 255, 488))
	TeamFuBen_BEIHAIDONGDING      = NewFubenPos(nil, action.NewPos("北海洞顶", 255, 638))
	//把4翻到1的位置
	toggle = func() {
		log.Infof("上拖列表中")
		mouse.Toggle(255, 638, 255, 203)
	}
	TeamFuBen_XIXIESHENGDIAN = NewFubenPos(toggle, action.NewPos("西邪圣殿", 255, 348))
	TeamFuBen_SHENGONG       = NewFubenPos(toggle, action.NewPos("神宫", 255, 488))
	TeamFuBen_SHANXISI       = NewFubenPos(toggle, action.NewPos("山夕寺", 255, 638))

	TeamFuBen_Items = []*FubenPos{
		TeamFuBen_XIEJIAOMIKU,
		TeamFuBen_BISHILIN,
		TeamFuBen_HUXUEBIEYUAN,
		//TeamFuBen_BEIHAIDONGDING,
		//TeamFuBen_XIXIESHENGDIAN,
		//TeamFuBen_SHENGONG,
		//TeamFuBen_SHANXISI,
	}

	SideButton_ZuDui = action.NewPos("侧边栏_组队", 1170, 330)
	KuaiSuJiaRu      = action.NewPos("快速加入", 1040, 670)
)

type FubenPos struct {
	Toggle func()
	Pos    *action.Pos
}

func NewFubenPos(toggle func(), pos *action.Pos) *FubenPos {
	return &FubenPos{
		Toggle: toggle,
		Pos:    pos,
	}
}

//组队副本
func RunTeamFuBen() {

	st := time.Now()
	defer func() {
		used := time.Now().Sub(st)
		usedSec := int64(used.Seconds())
		log.Infof("完成[%v]花费了%v分%v秒.", "组队副本", usedSec/60, usedSec%60)
	}()

	for _, p := range TeamFuBen_Items {
		for i := 0; i < 2; i++ {
			ok := counterSetTodaySomethingDone(fmt.Sprintf("%v_%v", p.Pos.Name, i))
			if !ok {
				log.Errorf("RunTeamFuBen 发现[%v][%v]已执行过。", p.Pos, i)
				continue
			}
			action.ClickPos(Detail)
			action.ClickPos(FuBen)
			action.ClickPos(SideButton_ZuDui)

			if p.Toggle != nil {
				p.Toggle()
			}

			action.ClickPos(p.Pos)
			action.ClickPos(KuaiSuJiaRu)

			commonWaitToQuit()

		}
	}

}
