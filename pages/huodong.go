package pages

import (
	"auto_task.com/base/mouse"
	"auto_task.com/base/utils"
)

type HuoDongPage struct {
}

func GetHuoDongPage() *HuoDongPage {
	return &HuoDongPage{}
}

func (h *HuoDongPage) Open() {
	defer utils.SleepSec(2)
	mouse.MoveAndClick(740, 80)
}

func (h *HuoDongPage) Close() {
	mouse.MoveAndClick(1100, 140)
}
