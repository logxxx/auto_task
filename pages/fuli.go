package pages

import "auto_task.com/base/action"

var (
	Fuli                 = action.NewPos("福利", 820, 71)
	Fuli_Close           = CommonClosePage
	Fuli_DailyAct        = action.NewPos("福利_每日活动", 206, 358)
	Fuli_DailyAct_OnLine = action.NewPos("在线奖励", 685, 159)
)

// 领取在线奖励
func RecvOnlineBonus() {
	OnLine10 := action.NewPos("在线10分钟", 888, 259)
	OnLine30 := action.NewPos("在线30分钟", 888, 368)
	OnLine60 := action.NewPos("在线60分钟", 888, 472)
	OnLine120 := action.NewPos("在线120分钟", 888, 574)
	Blank := action.NewPos("空白", 1076, 583)
	action.ClickPoss(
		Fuli,
		action.WaitPos("等待窗口打开", 3),
		Fuli_DailyAct,
		Fuli_DailyAct_OnLine,
	)
	for _, pos := range []*action.Pos{OnLine10, OnLine30, OnLine60, OnLine120} {
		action.ClickPos(pos)
		action.ClickPos(Blank)
	}
	action.ClickPos(Fuli_Close)
}
