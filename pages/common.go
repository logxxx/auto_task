package pages

import (
	"auto_task.com/base/action"
	"auto_task.com/base/machine"

	"auto_task.com/base/screen"
	"auto_task.com/base/utils/log"
	"context"
	"time"
)

var (
	CommonClosePage = action.NewPos("右上角的关闭按钮", 1113, 140)
)

// 等待战斗，直到结束
func commonWaitToQuit() {
	ctx, cancel := context.WithCancel(context.Background())
	go autoUseShenBin(ctx)
	go askMachine(cancel, 10*time.Minute)
	log.Infof("等待结束...")
	<-ctx.Done()
	//点击两张牌，再点击离开
	action.ClickPos(ChooseBonus1)
	//点一张就够了。现在没钱
	//action.ClickPos(ChooseBonus2)
	action.ClickPos(LeaveTask)
	time.Sleep(5 * time.Second)
	//结束时，画面在【日常试炼列表】
	action.ClickPos(ShiLian_Close)
	action.ClickPos(Detail_Close)
	action.ClickPos(TuoGuan)
	action.ClickPos(QiCheng)
}

func askMachine(cancelFunc context.CancelFunc, d time.Duration) {
	defer cancelFunc()

	tick := time.Tick(d)
	round := 0
	reportTimeTick := time.Tick(10 * time.Second)
	askMachineTick := time.Tick(3 * time.Second)
	for {
		select {
		case <-reportTimeTick:
			round++
			log.Printf("sleep %v/%v", round*10, int(d.Seconds()))
		case <-tick:
			return
		case <-askMachineTick:
			log.Infof("askMachine start!")
			screenData, err := screen.ShotAreaAndResize(0, 30, 1260, 740, 624, 366)
			if err != nil {
				log.Errorf("askMachine ShotAreaAndResize err:%v", err)
				continue
			}
			result, err := machine.AskMachine(screenData)
			if err != nil {
				log.Errorf("askMachine AskMachine err:%v len(data):%v", err, len(screenData))
				continue
			}
			log.Printf("askMachine result:%+v", result)

			if result != nil && result.Result {
				log.Printf("askMachine SUCC!")
				return
			}
		}

	}
}
