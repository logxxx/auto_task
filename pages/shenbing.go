package pages

import (
	"auto_task.com/base/action"
	"context"
	"log"
	"time"
)

func autoUseShenBin(ctx context.Context) {
	log.Printf("自动释放神兵技能 开始!")
	t := time.Tick(20 * time.Second)
	for {
		select {
		case <-t:
			t = time.Tick(5 * time.Second)
			action.ClickPos(action.NewPos("神兵技能", 860, 660))
		case <-ctx.Done():
			log.Printf("自动释放神兵技能 结束!")
			return
		}
	}
}
