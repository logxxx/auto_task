package pages

import (
	"auto_task.com/base/action"
	"auto_task.com/base/utils"
	"time"
)

//万寿阁
//周三周四 10:00~23:00
func WanShouGe() {

	if !utils.IsWednesday() && !utils.IsThursday() {
		return
	}

	WanShouGeItem := action.NewPos("万寿阁", 227, 555)
	BaoMing := action.NewPos("报名", 1000, 645)
	action.ClickPoss(
		Detail,
		ShiLian,
		WanShouGeItem,
		BaoMing)
	//等待...
	utils.SleepSec(2 * 60)
	//骑乘；托管；瞄准(一分钟瞄准一次)
	action.ClickPoss(QiCheng, TuoGuan, MiaoZhun)
	func() {
		//5分钟通关。
		deadline := time.Tick(5 * time.Minute)
		miaozhun := time.Tick(1 * time.Minute)
		for {
			select {
			case <-deadline:
				return
			case <-miaozhun:
				action.ClickPos(MiaoZhun)
			}
		}
	}()

	//通关后，直接回到主界面。托管即可。
	action.ClickPos(TuoGuan)

}
