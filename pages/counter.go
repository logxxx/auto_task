package pages

import (
	"auto_task.com/base/counter"
	"auto_task.com/base/utils"
	"auto_task.com/base/utils/log"
	"fmt"
	"time"
)

var (
	keyFarmToolHealth    = "farm_tool_health"
	keyPlantHarvestCount = "farm_plant_harvest_count"
	keyPlantHervestTime  = "farm_plant_harvest_time"
)

// 设置今天某件事干过了
func counterSetTodaySomethingDone(key string) bool {
	key = fmt.Sprintf("done:%v_%v", utils.FormatDateSafe(), key)
	ok, err := counter.GetCounter().SetNX(key, time.Now().Unix(), time.Hour*24).Result()
	if err != nil {
		return false
	}
	return ok
}

func setPlantHervestTime(t time.Time) {
	log.Infof("设置农作物收获时间:%v", t)
	counter.GetCounter().Set(keyPlantHervestTime, t.Unix(), t.Sub(time.Now()))
}

func getPlantHervestTime() time.Time {
	resp, _ := counter.GetCounter().Get(keyPlantHervestTime).Int64()
	if resp <= 0 {
		return time.Time{}
	}
	log.Infof("获取农作物收获时间:%v", time.Unix(resp, 0).Format("2006-01-02 15:04:05"))
	return time.Unix(resp, 0)
}

func setPlantHarvestCount(count int) {
	log.Infof("设置农作物可收获次数为:%v", count)
	counter.GetCounter().Set(keyPlantHarvestCount, count, 0)
}

func getPlantHarvestCount() int {
	resp, _ := counter.GetCounter().Get(keyPlantHarvestCount).Int64()
	log.Infof("当前农作物可收获次数为:%v", resp)
	return int(resp)
}

func updatePlantHarvestCount(delta int) int {
	resp, _ := counter.GetCounter().IncrBy(keyPlantHarvestCount, int64(delta)).Result()
	log.Infof("更新农作物可收获次数:%v 结果:%v", delta, resp)
	return int(resp)
}

func setFarmtoolHealth(health int) {
	log.Infof("设置锄头当前健康度为%v", health)
	counter.GetCounter().Set(keyFarmToolHealth, health, 0)
}

func getFarmtoolHealth() int64 {
	health, _ := counter.GetCounter().Get(keyFarmToolHealth).Int64()
	log.Infof("锄头当前健康度:%v", health)
	return health
}

func consumeFarmtool() {
	resp, _ := counter.GetCounter().IncrBy(keyFarmToolHealth, -1).Result()
	log.Infof("消耗1点锄头耐久度。剩余耐久度:%v", resp)
}
