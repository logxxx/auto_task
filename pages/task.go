package pages

import (
	"auto_task.com/base/action"
	"auto_task.com/base/mouse"
	"auto_task.com/config"

	"auto_task.com/base/utils/log"
	"fmt"
	"time"
)

var (
	Detail        = action.NewPos("详情模式", 80, 100)
	Detail_Close  = action.NewPos("详情模式_关闭", 30, 50)
	ShiLian       = action.NewPos("试炼", 100, 300)
	ShiLian_Close = action.NewPos("试炼_关闭", 1160, 110)
	QianWang      = action.NewPos("前往", 870, 620)
	TuoGuan       = action.NewPos("托管", 1200, 270)
	QiCheng       = action.NewPos("骑乘", 1100, 270)
	MiaoZhun      = action.NewPos("瞄准", 1200, 366)
	ShenBing      = action.NewPos("神兵", 857, 672)
	ChooseBonus1  = action.NewPos("选择奖励1", 612, 493)
	ChooseBonus2  = action.NewPos("选择奖励2", 882, 478)
	LeaveTask     = action.NewPos("离开", 1114, 667)
	Map           = action.NewPos("地图", 1220, 70)
	Map_World     = action.NewPos("世界地图", 198, 170)
)

type Task struct {
	Pos      *action.Pos
	Duration time.Duration
	PreFunc  func()
	Count    int
}

func NewTask(pos *action.Pos, duration time.Duration, preFunc func(), count int) *Task {
	return &Task{
		Pos:      pos,
		Duration: duration,
		PreFunc:  preFunc,
		Count:    count,
	}
}

func DefaultTask(pos *action.Pos, preFunc func()) *Task {
	return &Task{
		Pos:      pos,
		Duration: 3 * time.Minute,
		PreFunc:  preFunc,
		Count:    2,
	}
}

func GetTasks() []*Task {
	x := int(370)
	y := int(180)

	pos11 := action.NewPos("任务1_1位置", 720, 170)
	task11 := DefaultTask(pos11, nil)
	_ = task11
	task12 := DefaultTask(pos11.Copy("task12", x, 0), nil)
	_ = task12
	task21 := DefaultTask(pos11.Copy("task21", 0, y), nil)
	task22 := DefaultTask(pos11.Copy("task22", x, y), nil)
	task31 := DefaultTask(pos11.Copy("task31", 0, y*2), nil)
	task32 := DefaultTask(pos11.Copy("task32", x, y*2), nil)
	//把31 32移动到 11 12后
	toggle := func() {
		log.Printf("上拖列表中")
		mouse.Toggle(task31.Pos.X, task31.Pos.Y, pos11.X, pos11.Y)
	}
	task41 := DefaultTask(pos11.Copy("task41", 0, y), toggle)
	task42 := DefaultTask(pos11.Copy("task42", x, y), toggle)
	task51 := DefaultTask(pos11.Copy("task51", 0, y*2), toggle)
	task52 := DefaultTask(pos11.Copy("task52", x, y*2), toggle)
	toggle2 := func() {
		log.Printf("上拖列表中")
		mouse.Toggle(task31.Pos.X, task31.Pos.Y, pos11.X, pos11.Y)
		mouse.Toggle(task31.Pos.X, task31.Pos.Y, pos11.X, pos11.Y)
	}
	task61 := DefaultTask(action.NewPos("task61", 566, 484), toggle2)

	_ = task11
	_ = task12
	_ = task21
	_ = task22
	_ = task31
	_ = task32
	_ = task41
	_ = task42
	_ = task51
	_ = task52
	_ = task61

	tasks := []*Task{
		//task11,
		task12,
		task21,
		task22,
		//task31,
		//task32,
		//task41,
		//task42,
		//task51,
		//task52,
		//task61,
	}

	return tasks

}

func doTaskCore(task *config.Task) error {

	st := time.Now()
	defer func() {
		used := time.Now().Sub(st)
		usedSec := int64(used.Seconds())
		log.Infof("完成任务[%v]花费了%v分%v秒.", task.Name, usedSec/60, usedSec%60)
	}()

	action.ClickPos(Detail)
	action.ClickPos(ShiLian)

	actions, err := config.ParseCommands(task.Commands)
	if err != nil {
		log.Errorf("DoTask ParseCommands err:%v req:%v", err, task.Commands)
		return err
	}

	action.Do(actions...)

	action.ClickPos(QianWang)
	commonWaitToQuit()

	return nil
}

func DoTask() error {
	tasks := config.GetConfig().ShiLianTask.Tasks
	for _, task := range tasks {
		for i := 0; i < task.Count; i++ {
			if task.Skip {
				log.Printf("任务[%v]被标记为跳过", task.Name)
				continue
			}
			log.Printf("开始执行任务[第%v次]:%v", i+1, task.Name)
			ok := counterSetTodaySomethingDone(fmt.Sprintf("%v%v", task.Name, i))
			if !ok {
				log.Printf("任务[%v]已执行过，所以跳过。", task.Name)
				continue
			}
			err := doTaskCore(task)
			if err != nil {
				log.Errorf("DoTask doTaskCore err:%v req:%+v", err, task)
				return err
			}
		}
	}

	return nil

}
