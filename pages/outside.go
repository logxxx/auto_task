package pages

import (
	"auto_task.com/base/action"
	"context"
	"time"
)

// 永久野外挂机
func OutsizeHangup(ctx context.Context) {
	for {
		select {
		case <-time.After(10 * time.Second):
			action.ClickPos(ShenBing)
		case <-ctx.Done():
			return
		}
	}
}

func JumpToMonsterPoint() {

	var (
		HuaTingPingYuan  = action.NewPos("地图_华庭平原", 294, 443)
		Tab_MonsterPoint = action.NewPos("刷怪点", 1010, 194)
		Monster1         = action.NewPos("怪物1", 1043, 256)
		Monster2         = action.NewPos("怪物2", 1043, 316)
		Monster3         = action.NewPos("怪物3", 1043, 376)
		Monster4         = action.NewPos("怪物4", 1043, 426)
		Monster5         = action.NewPos("怪物5", 1043, 486)
		Monster6         = action.NewPos("怪物6", 1043, 546)
		Monster7         = action.NewPos("怪物7", 1043, 692)
		Monster8         = action.NewPos("怪物8", 1043, 648)
	)

	_ = Monster1
	_ = Monster2
	_ = Monster3
	_ = Monster4
	_ = Monster5
	_ = Monster6
	_ = Monster7
	_ = Monster8

	action.ClickPoss(
		Map,
		Map_World,
		HuaTingPingYuan,
		Tab_MonsterPoint,
		Monster8,
		action.WaitPos("loading", 5),
		TuoGuan,
	)
}
