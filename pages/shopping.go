package pages

import "auto_task.com/base/action"

var (
	Mall        = action.NewPos("商城", 889, 72)
	XianGou     = action.NewPos("限购", 865, 186)
	BuyCountMax = action.NewPos("购买_数量最大", 764, 456)
	BuyCommit   = action.NewPos("购买_确定", 731, 596)
	Mall_Close  = action.NewPos("商城_关闭", 1173, 109)
)

func Malling() {
	var (
		MallItem1 = action.NewPos("胜者升旗", 991, 522)
		//MallItem2 = NewPos("鸡肉", 242, 674)
		//MallItem3 = NewPos("白菜", 242, 674)
		//MallItem4 = NewPos("豆腐", 242, 674)
		//MallItem5 = NewPos("南瓜", 242, 674)
		//MallItem6 = NewPos("鸡肉", 242, 674)
		//MallItem7 = NewPos("鸡肉", 242, 674)
	)

	action.ClickPoss(
		Mall,
		XianGou,
		MallItem1,
		BuyCountMax,
		BuyCommit,
		Mall_Close,
	)
}
